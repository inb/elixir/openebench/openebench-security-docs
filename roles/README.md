# Privileges associated to OEB user roles

## OpenEBench Roles permissions

CRUD: **C**reate, **R**ead, **U**pdate, **D**elete, **d**elete (when it is not public), **r**ead (depends on `dates.public`)

Visibility: Community (Y), Challenge (X), Public (P)

|                   | owner (community) | manager (benchmarking event) | contributor (benchmarking event) | contributor (challenge) | Public | Anyone |
|-------------------|------|------|------|------|------|------|
| Community         |  RU  |  R   |  R   |  R   |  R   |  R   |
| BenchmarkingEvent | CRUD |  RU  |  R   |  R   |  R   |  R   |
| Challenge         | CRUD | CRUd |  R   |  R   |  R   |  R   |
| Dataset (P) (owns)|  R   |  R   | CRUd | CRUd |  v   |  v   |
| Dataset (R)       | CRUD | CRUd |  R   |  R   |  v   |  v   |
| Dataset (A)       | CRUD | CRUd |  r   |  r   |  v   |  v   |
| Metrics           | CRUD | CRUd |  R   |  R   |  R   |  R   |
| Contact           | CRUD |  R   |  R   |  R   |      |      |
| Reference         | CRUd | CRUd |  R   |  R   |  R   |  R   |
| TestAction        | CRUD | CRUd |  r   |  r   |  v   |  v   |
| Tool              | CRUd | CRUd |  R   |  R   |  R   |  R   |

## Roles
| --- Community Owner

| - - |__ Benchmarking Event Manager

| - - |__ Benchmarking Event Contributor

| - - - - |__ Challenge Contributor

| - - - - - - |__ ~~Challenge Participant~~

| - - - - - - - - |__ Public (default role)

| - - - - - - - - - -  Anyone (not registered)

| - - - - |__ Challenge Evaluator

## OpenEBench Secured Data Collections
The current implementation of OpenEBench REST-API uses a set of "authorizers" - classes that implement the decision whether read/write operation is permitted.
Each such class implements isReadable(Document doc)/isWritable(Document doc). Note that in some documents, for instance Dataset, the decision depends on the 
document itself (i.e. "visibility": "public"). Write permissions are even more complicated (may a challenge contributor submit a dataset as "public" document?).  
Here are detailed security rules for authorization permission checks:  
'admin:oeb' - is a special role for the unrestricted access.

1. **Privilege**  
   This collection is private and must not be accessible to users with an exception of the "oeb:admin".  
2. **Contact**  
   Q. Who may write contacts (ContactAuthorization.isWritable())?  
      ``"community owner"          - [?]``  
      ``"challenge manager"        - [?]``  
      ``"challenge contributor"    - [?]``  
      ``"challenge participant"    - [?]``  
   Q. Who may read contacts (ContactAuthorization.isReadable())?  
      ``"community owner"          - [?]``  
      ``"challenge manager"        - [?]``  
      ``"challenge contributor"    - [?]``  
      ``"challenge participant"    - [?]``  
   **N.B.** Should we check whether the contact is actually reffered from a collection, user has an access?
3. **Community**  
   Only users in "oeb:admin" roles can read/write the collection  
4. **BenchmarkingEvent**  
   Q. Who may write events (BenchmarkingEventAuthorization.isWritable())?  
      ``"community owner"          - [+]`` (only "owner:community_id" of the event)  
   Q. Who may read events (BenchmarkingEventAuthorization.isReadable())?  
      ``"community owner"          - [+]``  
      ``"challenge manager"        - [+]``  
      ``"challenge contributor"    - [+]``  
      ``"challenge participant"    - [+]``  
      ``"anonymous"                - [?]``  
5. **Challenge**  
   Q. Who may write challenges (ChallengeAuthorization.isWritable())?  
      ``"community owner"          - [+]`` (only "owner:community_id" of the challenge)  
   Q. Who may read challenges (ChallengeAuthorization.isReadable())?  
      ``"community owner"          - [+]``  
      ``"challenge manager"        - [+]``  
      ``"challenge contributor"    - [+]``  
      ``"challenge participant"    - [+]``  
      ``"anonymous"                - [?]``  

##  

1. 'Privilege' only users in role 'admin:oeb' allowed to see.
2. 'Contact' only 'admin:oeb' for now. ? 
3. 'Dataset' 
        visiblity: 'public' - everyone. 
        visibility: 'participant' - community owners ('owner:OEBC002'), challenge contributors and managers (i.e. 'manager:OEBC00200001FO')
        visibility: 'challenge' - community owners and challenge managers
        visibility: 'community' - community owners
4. 'Challenge' community owners, challenge contributors and managers
5. 'Community'
6. 'Metrics'
7. 'References'

## Assumptions (2nd round Oct16)
1. All "owner"s are Community owners.  
   Owners have full access to the community they own and correspondent challenges.
2. Challenges have different access roles:  
   "manager" == "owner" on the challenge level.  
   "contributor" - may contribute (submit) data for the challenge  
   "member" - readonly access only for the "challenge" it is a member (???!!!)
3. "public" - is automatically access level for all registered KC users.

## Mongo Data Model (3d round > Oct16)
All security data is stored in the 'Privilege' collection [privilege.json](https://github.com/inab/benchmarking-data-model/blob/1.1.x-future/json-schemas/1.0.x/privilege.json).  
The "_id" of the 'Privilege' collection is the "_id" of 'Contact'.  
The "roles" attribute refers either to "community_id" or "challenge_id".  

## Keycloak model (3d round > Oct16)
All "roles" are modelled via "attributes" in a form of "role:id".  
Challenge-related roles inserted directly as "user.attributes" (i.e. "manager:challenge_id").  
Community-related roles automatically assigned when user inserted in the correspondent "Community" group.  
Every user added in the community group becames an "owner" of this community.  
Finally, all attributes inserted into the oidc token as an array "oeb:roles": ["owner:community_id","manager:challenge_id", ...]  

## Mongo Data Model (2nd round Oct16)
4. Community.~~community_contact_ids~~[]  
   **Community.community_owner_ids**[]  
   Because only "community" level users are owners,
   these contacts are in (communnity) "owner" role.
5. Challenge.~~challenge_contact_ids~~[]
~~~json
"roles" : {
  "title": "Challenge roles assigned to this contact",
  "type": "array",
  "items": {
    "type": "object",
    "properties": {
      "role": {
        "title": "Challenge role assigned to the contact",
        "type": "string",
        "enum":  [ "manager", "contributor", "member" ]
      },
      "contact_id": {
        "title": "The challenge contact id(s)",
        "type": "string",
        "foreign_keys": [{
          "schema_id": "Contact",
          "members": [ "." ]
        }],
        "minLength": 1
      }
    }
  }
}
~~~
## Assumptions
* OpenEBench services access to a central user's profile via OIDC, either using the `/userinfo` endpoint or as part of the ID token.
* User's profile data will correspond to a list of *claims* (under "OEB" scope?):
    * **community_id** (array) 
    * **challengues_id** (array) 
    * **roles** (array). Corresponds to the combination of the roles above defined and the communities/challengues that role applies to. For instance : `public manager:OEBC001 member:OEBC002:CH004 member:OEBC002:CH005`
    *  **community_acronyms** (array)
    * [...]

# Keycloak Groups and Roles
* Keycloak 'roles' came frome JavaEE 'roles' concept and are global. Groups can have fixed roles assigned to the users they contain, 
  but assigned role have no connection with groups.
* The current idea is to use groups to model security via fixed attributes assigned to the group.
  Groups may have subgroups so the full group 'path' looks like 'Community/Quest for Orthologs/Species Three Discordance in LUCA benchmark/manager'
  Note that **challenge.name** MUST be unique within a community.
* Every 'leaf' group is actually a **role** which would contain an attribute with a correspondent 'role' and its value set to 'community_acronym/challenge_acronym'.

# Privilege tables

### .. for accessing OEB datasets (?)

| **role**     | public | community | challenge | participant| protected (?)|
|--------------|--------|-----------|-----------|------------|--------------|
| owner        | rw     | rw        | rw        | rw         | rw           | 
| manager      | rw     | rw        | rw        | rw         | rw           | 
| contributor  | r      | r         | r         | rw         | -            | 
| member       | r      | r         | -         | -          | -            | 
| public       | r      | -         | -         | -          | -            | 


### ... for VRE operations

| **privileged resouce** | **access level** | owner role | manager role | contributor role | member role| (public) |
|------------------------|------------------|------------|--------------|------------------|------------|----------|
|                        | public           |     rwx    |  rwx         |  rx              |  rx        |  rx      |
| **WF/Blocks**          | community        |     rwx    |  rwx         |  rx              |  rx        |          |
|                        | challenge        |     rwx    |  rwx         |  rx              |  rx        |          |
|________________________| _________________|____________|______________|__________________|____________|__________|
|                        | public           |     rwx    |  rx          |  rx              |  rx        |  rx      |
| **Tools**              | community        |     rwx    |  rx          |  rx              |  rx        |          |
|                        | challenge        |     rwx    |  rx          |  rx              |  rx        |          |
|                        | ~testing~        |    ~rwx~   |  ~rwx~       |  ~n/a~           |  ~n/a~     |          |
|                        | ~comming_soon~   |    ~rw~    |  ~rw~        |  ~n/a~           |  ~n/a~     | ~r~      |
|________________________| _________________|____________|______________|__________________|____________|__________|
|Users admin. panel      | -                |     x      |              |                  |            |          |
|Job admin. panel        | -                |     x      |              |                  |            |          |
|Job Logging panel       | -                |     x      |              |                  |            |          |
|Tool admin. panel       | -                |     x      |      x       |                  |            |          |
|Developer mode(*)       | -                |     x      |      x       |          x       |            |          |


(*) *Developers mode: view files' (raw) metadata, job-related data (submission file, output's metadata)*


### ... for  Docker-registry operations

|**privileged resouce** | **access level** | owner role | manager role | contributor role | member role| (public) |
|-----------------------|------------------|------------|--------------|------------------|------------|----------|
|                       | ~public~         |    ~rw~    |  ~rw~        |  ~rw~            |  ~rw~      |  ~rw~    |
| **Docker images(*)**  | community        |     rx     |  rwx         |  rx              |  rx        |          |
|                       | challenge        |     rx     |  rwx         |  rx              |  rx        |          |

(*) *Current model: images.openebench.bsc.es/community/challenge/image:tag*



